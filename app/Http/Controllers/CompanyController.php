<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompanyRequest;
use App\Models\Company;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\View\View;

class CompanyController extends Controller
{
    public function index(): View
    {
        $companies = Company::with('employees')->get();

        return view('companies.index', compact('companies'));
    }

    public function create(): View
    {
        return view('companies.create');
    }

    public function store(CompanyRequest $request): RedirectResponse
    {
        if($request->hasFile('logo')) {
            $logo = $request->file('logo')->store('public');
        }

        $company = Company::create([
            'name' => $request->name,
            'email' => $request->email,
            'logo' => $logo ??= null,
            'website' => $request->website,
        ]);
        if (!$company) {
            return redirect()->back()->with('error', 'Not success');
        }

        return redirect()->route('companies.index')->with('success', 'Company created successfully');
    }

    public function edit(Company $company)
    {
        return \view('companies.edit', compact('company'));
    }

    public function update(Request $request, Company $company)
    {
        if($request->hasFile('logo')) {
            if($company->logo) {
                Storage::delete($company->logo);
            }
            $logo = $request->file('logo')->store('public');
        }
        $company->update([
            'name' => $request->name,
            'email' => $request->email,
            'logo' => $logo ??= null,
            'website' => $request->website,
        ]);
        if (!$company) {
            return redirect()->back()->with('error', 'Not success');
        }

        return redirect()->route('companies.index')->with('success', 'Company updated successfully');
    }

    public function destroy(Company $company)
    {
        $company->remove();

        return redirect()->route('companies.index')->with('success', 'Company deleted successfully');
    }
}
