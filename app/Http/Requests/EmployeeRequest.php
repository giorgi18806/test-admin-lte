<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $id = optional($this->employee)->id;
        return [
            'first_name' => 'required|string|min:3',
            'last_name' => 'required|string|min:3',
            'company_id' => 'required|numeric',
            'email' => 'required|email|unique:employees,email,'.$id,
        ];
    }
}
