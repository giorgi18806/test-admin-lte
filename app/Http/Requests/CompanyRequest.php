<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $id = optional($this->company)->id;
        return [
            'name' => 'required|string|min:3',
            'email' => 'required|email|unique:companies,email,'.$id,
            'logo' => 'required|mimes:png,jpg,jpeg,webp',
            'website' => 'nullable|string'
        ];
    }
}
