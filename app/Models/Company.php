<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Storage;

class Company extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'email',
        'logo',
        'website'
    ];

    public function employees(): HasMany
    {
        return $this->hasMany(Employee::class);
    }

    public function remove()
    {
        Storage::delete($this->logo);
        $this->delete();
    }

}
