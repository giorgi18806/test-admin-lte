@extends('layouts.app')

@section('title', 'Create New Company')
@section('content-header', 'Create New Company')

@section('content-action')
    <a href="{{ route('companies.index') }}" class="btn btn-dark">Go back</a>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="table-responsive-lg">
                <form action="{{ route('companies.update', $company) }}" class="row g-3 needs-validation" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="col-md-4">
                        <label for="name" class="form-label">Company title</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Input company title" value="{{ old('name', $company) }}">
                        @error('name') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="col-md-4">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Input email" value="{{ old('email', $company) }}">
                        @error('email') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="col-md-4">
                        <label for="website" class="form-label">Website</label>
                        <input type="website" class="form-control" id="website" name="website" placeholder="Input website" value="{{ old('website', $company) }}">
                        @error('website') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    @if($company->logo)
                        <img src="{{ Storage::url($company->logo) }}" style="width: 100px; height: 100px;" alt="Company logo">
                    @endif
                    <div class="col-md-4">
                        <label for="logo">Upload Logo</label>
                        <input type="file" class="form-control" name="logo" id="logo">
                    </div>
                    <div class="text-right col-12">
                        <button class="btn btn-dark">
                            <i class="fa-solid fa-pen-to-square"></i> Update
                        </button>
                        <a href="{{ route('companies.index') }}" class="btn btn-danger">
                            <i class="fa fa-close"></i> Close
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
