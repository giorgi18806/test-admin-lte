@extends('layouts.app')

@section('title', 'Company List')
@section('content-header', 'Company list')

@section('content-action')
    <a href="{{ route('companies.create') }}" class="btn btn-dark">Add New Company</a>
@endsection

@section('content')

    <div class="card">
        <div class="card-body">
            <div class="table-responsive-lg">
                <table id="company-table" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Logo</th>
                        <th>Website</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($companies as $company)
                    <tr>
                        <td>{{ $company->id }}</td>
                        <td>{{ $company->name }}</td>
                        <td>{{ $company->email }}</td>
                        <td><img src="{{ Storage::url($company->logo) }}" style="width: 100px; height: 100px;" alt="Company logo"></td>
                        <td>{{ $company->website }}</td>
                        <td>
                            <a href="{{ route('companies.edit', $company) }}" class="btn btn-dark">Edit</a>
                            <a href="#" class="btn btn-danger" onclick="document.getElementById('delete-form').submit()">
                                Delete
                                <form action="{{ route('companies.destroy', $company) }}" method="POST" id="delete-form">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection

@push('scripts')
    <script>
        $(function () {
            $('#company-table').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
@endpush
