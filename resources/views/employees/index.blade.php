@extends('layouts.app')

@section('title', 'Employee List')
@section('content-header', 'Employee list')

@section('content-action')
    <a href="{{ route('employees.create') }}" class="btn btn-dark">Add New Employee</a>
@endsection

@section('content')
    <p class="login-box-msg">@include('messages.flash-message')</p>
    <div class="card">
        <div class="card-body">
            <div class="table-responsive-lg">
                <table id="employee-table" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Company</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($employees as $employee)
                        <tr>
                            <td>{{ $employee->id }}</td>
                            <td>{{ $employee->first_name }}</td>
                            <td>{{ $employee->last_name }}</td>
                            <td>{{ $employee->company->name }}</td>
                            <td>{{ $employee->email }}</td>
                            <td>{{ $employee->phone }}</td>
                            <td>
                                <a href="{{ route('employees.edit', $employee) }}" class="btn btn-dark">Edit</a>
                                <a href="#" class="btn btn-danger" onclick="document.getElementById('delete-form').submit()">
                                        Delete
                                    <form action="{{ route('employees.destroy', $employee) }}" method="POST" id="delete-form">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </a>
{{--                                <a href="{{ route('employees.destroy', $employee) }}" class="btn btn-danger">Delete</a>--}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(function () {
            $('#employee-table').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
@endpush
