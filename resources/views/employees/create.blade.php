@extends('layouts.app')

@section('title', 'Create New Employee')
@section('content-header', 'Create New Employee')

@section('content-action')
    <a href="{{ route('employees.index') }}" class="btn btn-info">Go back</a>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="table-responsive-lg">
                <form action="{{ route('employees.store') }}" class="row g-3 needs-validation" method="POST">
                    @csrf
                    <div class="col-md-4">
                        <label for="first_name" class="form-label">First name</label>
                        <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter first name" value="{{ old('first_name') }}">
                        @error('first_name') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="col-md-4">
                        <label for="last_name" class="form-label">Last name</label>
                        <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Enter last name" value="{{ old('last_name') }}">
                        @error('last_name') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="col-md-4">
                        <label for="company" class="form-label">Company</label>
                        <select class="form-control" name="company_id" id="company">
                            <option value="">Select company</option>
                            @foreach($companies as $company)
                                <option value="{{ $company->id }}">{{ $company->name }}</option>
                            @endforeach
                        </select>
                        @error('company_id') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="col-md-4">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" value="{{ old('email') }}">
                        @error('email') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="col-md-4">
                        <label for="phone" class="form-label">Phone</label>
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Enter phone" value="{{ old('phone') }}">
                        @error('email') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="text-right col-12">
                        <button class="btn btn-primary">
                            <i class="fa fa-plus"></i> Submit
                        </button>
                        <a href="{{ route('employees.index') }}" class="btn btn-danger">
                            <i class="fa fa-close"></i> Close
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
